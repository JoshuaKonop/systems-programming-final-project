#ifndef __RMDIR_C__
#define __RMDIR_C__

#include "type.h"
#include "util.c"

int rm_child(MINODE *pmip, char *name)
{
  int i, j;
  char buf[BLKSIZE], temp[MAX];
  char *cpy_start, *cpy_end, *cp, *last_cp;
  DIR *dp, *last_dp, *before_last_dp;

  for(i=0;i<12;i++)
    {
      if(pmip->INODE.i_block[i] != 0)
	{
	  get_block(globalDev, pmip->INODE.i_block[i], buf);
	  dp = (DIR *)buf;
	  cp = buf;

	  while(cp < buf + BLKSIZE)
	    {
	      strncpy(temp, dp->name, dp->name_len);
	      temp[dp->name_len] = 0;
	      if(strcmp(temp, name) == 0)
		{
		  if(cp == buf && dp->rec_len == buf + BLKSIZE) //first
		    { 
		      bdalloc(globalDev, pmip->INODE.i_block[i]);
		      pmip->INODE.i_size -= BLKSIZE;

		      for(j=i;j<12;j++)
			{
			  if(pmip->INODE.i_block[j+1] == 0)
			    break;
			  get_block(globalDev, pmip->INODE.i_block[j], buf);
			  put_block(globalDev, pmip->INODE.i_block[j-1], buf);
			}
		    }
		  else if(cp + dp->rec_len == buf + BLKSIZE) //last
		    {
		      before_last_dp->rec_len += dp->rec_len;
		      put_block(globalDev, pmip->INODE.i_block[i], buf);
		    }
		  else //in between
		    {
		      last_dp = (DIR *)buf;
		      last_cp = buf;
		      while(last_cp + last_dp->rec_len < buf + BLKSIZE)
			{
			  last_cp += last_dp->rec_len;
			  last_dp = (DIR *)last_cp;
			}
		      last_dp->rec_len += dp->rec_len;

		      cpy_start = cp + dp->rec_len;
		      cpy_end = buf + BLKSIZE;

		      int size = cpy_end - cpy_start;
		      //book pg. 340 | move entries left
		      //      dst    src      size
		      memmove(cp, cpy_start, size);
		      put_block(globalDev, pmip->INODE.i_block[i], buf);
		    }
		  return 1;
		}
	      before_last_dp = dp;
	      cp += dp->rec_len;
	      dp = (DIR *)cp;
	    }
	}
  }
  return -1;
}

int remove_dir(char *pathname)
{
  int ino, pino, check;
  MINODE *mip, *pmip;
  char name[MAX];

  if(!strcmp(pathname, "") || !strcmp(pathname, "..") || !strcmp(pathname, "."))
    {
      printf("Invalid Input\n");
      return -1;
    }

  ino = getino(pathname);
  mip = iget(globalDev, ino);
  check = empty(mip);

  if (mip->INODE.i_uid != running->uid && running->uid != 0) {
    printf("Not file owner!\n");
    return -1;
  }

  if((mip->INODE.i_mode & 0xF000) != 0x4000) // if not dir
    {
      printf("Error, %s is not a directory\n", pathname);
      iput(mip);
      return -1;
    }
  if(mip->refCount > 1) // if busy
    {
      printf("Error, Inode is in use\n");
      iput(mip);
      return -1;
    }
  if(check == -1) // doesn't work, i'll fix it later
    {
      printf("%s is not empty\n", pathname);
      iput(mip);
      return -1;
    }

  pino = search(mip, "..");
  pmip = iget(mip->dev, pino);

  char *child = basename(pathname);

  check = rm_child(pmip, child);
  if(check == -1)
    {
      printf("Error, rm_child failed\n");
      iput(pmip);
      iput(mip);
      return -1;
    }

  pmip->INODE.i_links_count--;
  pmip->dirty = 1;
  iput(pmip);

  bdalloc(mip->dev, mip->INODE.i_block[0]);
  idalloc(mip->dev, mip->ino);
  mip->dirty = 1;
  iput(mip);

  return 1;
}

int empty(MINODE *mip)
{
  int i = 0;
  char buf[BLKSIZE];
  char *cp;
  DIR *dp;

  if(mip->INODE.i_links_count > 2)
  {
    return -1;
  }
  else if(mip->INODE.i_links_count == 2)
    {
      get_block(mip->dev, mip->INODE.i_block[0], buf);
      dp = (DIR *)buf;
      cp = buf;

      while (cp < buf + BLKSIZE)
	{
	cp += dp->rec_len;
	dp = (DIR *)cp;
	i++;
	}
      if(i>2)
	{
	  return -1;
	}
    }
  return 1;
}

#endif
