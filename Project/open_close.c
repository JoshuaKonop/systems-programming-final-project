/************* open_close_lseek.c file **************/

#ifndef __OPENCLOSELSEEK_C__
#define __OPENCLOSELSEEK_C__

#include "mkdir_creat.c"

int truncate(MINODE *mip)
{
  int in_buf[256], d_in_buf[256];
  for (int i = 0; i < 12; i++) {
    //Free iblock
    int blk = mip->INODE.i_block[i];
    bdalloc(globalDev, blk);

    mip->INODE.i_block[i] = 0;
  }
  //printf("HELP ME\n");
  for (int i = 12; i < 256 + 12; i++) {

    get_block(mip->dev, mip->INODE.i_block[12], (char*)in_buf);

	  int blk = in_buf[i-12];
    bdalloc(globalDev, blk);

    in_buf[i - 12] = 0;

	}
  mip->INODE.i_block[12] = 0;
  //printf("HELP ME2\n");
  //Free **iblock7
  get_block(globalDev, mip->INODE.i_block[13], (char*)in_buf);
  for (int i = 0; in_buf[i] != 0; i++) {
    get_block(mip->dev, in_buf[i], (char*)d_in_buf);
    for (int j = 0; j < 256; j++) {
      int blk = d_in_buf[j];
      bdalloc(globalDev, blk);

      d_in_buf[i - 12] = 0;
    }
    
  }
  mip->INODE.i_block[13] = 0;
  printf("HELP ME3\n");

  mip->INODE.i_size = 0;
  mip->dirty = 1;
  iput(mip);
  return 1;
}

int open_file(char *pathname, int mode)
{
  printf("pathname=%s mode=%d\n", pathname, mode); 
  int ino = getino(pathname);
  if (ino == 0) {
    creat_file(pathname);
    ino = getino(pathname);
  }
  printf("Found file ino=%d\n", ino);
  int i = 0;
  for (i; name[i + 1] != 0; i++) {}
  char mode2 = '0';
  if (mode == 0) mode2 = 'r';
  if (mode == 1) mode2 = 'w';
  if (access(name[i], mode2) == 0) {
    return -1;
  }

  MINODE* mip = iget(globalDev, ino);
  OFT* newOFT = malloc(sizeof(OFT));
  newOFT->mode = mode;
  printf("changing mode to %d\n", mode);
  newOFT->minodeptr = mip;
  printf("giving MINODE=%d\n", mip->ino);
  newOFT->offset = 0;
  if (mode == 3) {
    newOFT->offset = mip->INODE.i_size;
  }
  printf("Offset is %d\n",newOFT->offset);
  newOFT->refCount = 1;
  
  i = 0;
  for (; running->fd[i] != 0; i++){ if (i == 15) return -1;}
  printf("Inserting into fd[%d]\n", i);
  running->fd[i] = newOFT; 

  mip->INODE.i_atime == (unsigned int)time(0L);
  if (mode != 0) {
    mip->INODE.i_mtime == (unsigned int)time(0L);
  }

  mip->dirty = 1;
  return i; // Eventually: return file descriptor of opened file
}

int my_close(int fd)
{
  printf("Closing fd at %d\n", fd);
  if (fd > 15) {
    printf("NOT A VALID FILE DESCRIPTOR\n");
    return -1;
  }
  OFT* otfp;
  if (running->fd[fd]) {
    otfp = running->fd[fd];
    printf("Removing fd for ino=%d\n",otfp->minodeptr->ino);
    running->fd[fd] = 0;
    otfp->refCount--;
    if (otfp->refCount > 0) {
      return 0;
    }
    printf("REMOVING OFT FOR FILE\n");
    MINODE *mip = otfp->minodeptr;
    iput(mip);
  }
  
  return 0;
}

// int close_file(int fd)
// {
//   return 0;
// }

int my_lseek(int fd, int position)
{
  if (running->fd[fd] == 0) {
    printf("FILE AT FD DOES NOT EXIST\n");
    return -1;
  }

  if (position > running->fd[fd]->minodeptr->INODE.i_size) {
    printf("SELECTED POSITION IS GREATER THAN FILE SIZE\n");
    return -1;
  }

  int orig = running->fd[fd]->offset;
  running->fd[fd]->offset = position;

  return orig; // Eventually: return original position in file
}

int pfd()
{
  int i = 0;
  for (; i < 16; i++) {
    if (running->fd[i] != 0) 
      printf("fd=%d\nmode=%d\noffset=%d\nINODE=[%d, %d]\n\n",i, running->fd[i]->mode, running->fd[i]->offset,globalDev ,running->fd[i]->minodeptr->ino);
  }
    return 1;
}

int dup(int fd)
{
  if (fd > 15) {
    printf("NOT A VALID FILE DESCRIPTOR\n");
    return -1;
  }

  if (running->fd[fd] != 0) {
    int i = 0;
    OFT* oftp = running->fd[fd];
    for (; running->fd[i] != 0; i++){ if (i == 15) return -1;}
      running->fd[i] = oftp;
      oftp->refCount++; 
  }
  return 0;
}

int dup2(int fd, int gd)
{
  if (fd > 15 || gd > 15) {
    printf("NOT A VALID FILE DESCRIPTOR\n");
    return -1;
  }

  if (running->fd[gd] != 0) {
    my_close(gd);
  }
  if (running->fd[fd] != 0) {
    running->fd[gd] = running->fd[fd];
    running->fd[fd]->refCount++;
  }
  return 0;
}

#endif