/************* read_cat.c file **************/

#ifndef __READCAT_C__
#define __READCAT_C__

#include "open_close.c"

int read_file(int fd, char *buf, int nbytes)
{
  int i;

  if(running->fd[fd] == 0)
    {
      printf("Error, file is not open\n");
      return -1;
    }
  if(running->fd[fd]->mode == 1 || running->fd[fd]->mode == 3)
    {
      printf("Error, file is not open for R or RW\n");
      return -1;
    }

  i = my_read(fd, buf, nbytes);
  return i; // Eventually: Return the results of my_read
}

int my_read(int fd, char *buf, int nbytes)
{
  int count = 0, blk, lbk, startByte, avil, remain, filesize;
  char readbuf[BLKSIZE], *cq = buf;
  int in_buf[256], d_in_buf[256];
  OFT *oft = running->fd[fd];
  MINODE* mip = oft->minodeptr;

  filesize = mip->INODE.i_size;
  avil = filesize - oft->offset;

  while(nbytes > 0 && avil > 0)
    {
      lbk = oft->offset / BLKSIZE;
      startByte = oft->offset % BLKSIZE;

      if(lbk < 12) //direct
	{
	  blk = mip->INODE.i_block[lbk];
	}
      else if(lbk >= 12 && lbk < 12 + 256) //indirect
	{
	  get_block(mip->dev, mip->INODE.i_block[12], (char*)in_buf);

	  blk = in_buf[lbk-12];
	}
   else if (lbk >= (12 + 256)) {
      get_block(mip->dev, mip->INODE.i_block[13], (char*)in_buf);
      int i = (lbk - 12 - 256) / 256;
      int j = (lbk - 12 - 256) % 256;
      get_block(mip->dev, in_buf[i], (char*)d_in_buf);
      blk = d_in_buf[j]; 
   }
      /*else //double indirect
	{
	  //still can't figure it out
	}*/

      get_block(mip->dev, blk, readbuf);

      char *cp = readbuf + startByte;
      remain = BLKSIZE - startByte; // n of bytes remain at readbuf[]

      while(remain > 0)
	{
	  *cq++ = *cp++; //copy byte from readbuf into buf
	  oft->offset++; //advance offset
	  count++; //count n of bytes read
	  avil--; nbytes--; remain--;
	  if(nbytes <= 0 || avil <= 0)
	    break;
	}
      //if one data block is not enough, loop back to outer while for more
    }
  //printf("myread: read %d char from file descriptor %d\n", count, fd);
  return count; // Eventually: Return the actual number of bytes read
}

int cat_file(char *pathname)
{
  char mybuf[BLKSIZE], dummy = 0; // a null at the end of mybuf
  int n;
  char *cp;

  int fd = open_file(pathname, 0);

  while(n = my_read(fd, mybuf, BLKSIZE))
    {
      mybuf[n] = dummy; //make mybuf a null terminated string
      cp = mybuf;
//     printf("\n");
      while(*cp != '\0')
	{
	  if(*cp == '\n')
	    printf("\n");
	  else
	    printf("%c", *cp);
	  cp++;
	}
//	printf("\n");
//	printf("\n");
	//printf("%s\n", mybuf);
    }

  my_close(fd);
  return 1;
}

#endif
