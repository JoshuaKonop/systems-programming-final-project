/************* misc1.c file **************/

#ifndef __MISC1_C__
#define __MICS1_C__

#include "write_cp.c"

int menu()
{
    printf("\t\t******************** Menu *******************\n");
    printf("\t\tmkdir\tcreat\tmount\tumount\trmdir\n");
    printf("\t\tcd\tls\tpwd\tstat\trm\n");
    printf("\t\tlink\tunlink\tsymlink\tchmod\tchown\ntouch\n");
    printf("\t\topen\tpfd\tlseek\tclose\n");
    printf("\t\tread\twrite\tcat\tcp\tmv\n");
    printf("\t\tcs\tfork\tps\tkill\tquit\n");
    printf("\t\t=============   Usage Examples ==============\n");
    printf("\t\tmkdir\tfilename\n");
    printf("\t\tmount\tfilesys\t/mnt\n");
    printf("\t\tchmod\tfilename\t0644\n");
    printf("\t\topen\tfilename\tmode (0|1|2|3 for R|W|RW|AP)\n");
    printf("\t\twrite\tfd\ttext_string\n");
    printf("\t\tread\tfd\tnbytes\n");
    printf("\t\tpfd\t(display opened file descriptors)\n");
    printf("\t\tcs\t(switch process)\n");
    printf("\t\tfork\t(fork child process)\n");
    printf("\t\tps\t(show process queue as Pi[uid]==>)\n");
    printf("\t\tkill\tpid\t(kill a process)\n");
    printf("\t\t*********************************************\n");
}

struct stat my_stat(char *pathname)
{
    struct stat myst;

    int ino = getino(pathname);
    MINODE* mip = iget(globalDev, ino);
    INODE* ip = &mip->INODE;

    time_t x = ip->i_atime;
    struct timespec y;
    y.tv_sec = x;
    y.tv_nsec = ip->i_atime;
    myst.st_atim = y;

    myst.st_blksize = ip->i_block;
    myst.st_blocks = ip->i_blocks;
    myst.st_dev = mip->dev;
    myst.st_gid = ip->i_gid;
    myst.st_ino = ino;
    myst.st_mode = ip->i_mode;

    x = ip->i_mtime;
    y.tv_sec = x;
    y.tv_nsec = ip->i_mtime;
    myst.st_mtim = y;

    x = ip->i_ctime;
    y.tv_sec = x;
    y.tv_nsec = ip->i_ctime;
    myst.st_ctim = y;
    
    myst.st_nlink = ip->i_links_count;
    myst.st_rdev = mip->dev;            //Double check if this is correct
    myst.st_size = ip->i_size;
    myst.st_uid = ip->i_uid;
    iput(mip);
    return myst;
}

int my_chmod(char *pathname, int mode)
{
    int ino = getino(pathname);
    MINODE *mip = iget(globalDev, ino);

    mip->INODE.i_mode |= mode;
    mip->dirty = 1;

    iput(mip);
    return 1;
}

int my_utime(char *pathname)
{
    int ino = getino(pathname);
    MINODE *mip = iget(globalDev, ino);
    printf("Got ino=%d\n",ino);

    mip->INODE.i_mtime = (unsigned int)time(0L);
    mip->INODE.i_atime = (unsigned int)time(0L);
    mip->dirty = 1;

    iput(mip);
    return 1;
}

int my_chown(char *pathname, int uid)
{
    int ino = getino(pathname);
    MINODE *mip = iget(globalDev, ino);

    mip->INODE.i_uid = uid;
    mip->dirty = 1;

    iput(mip);
    return 1;
}

int cs()
{
  return 1;
}

int fork()
{
  return 1;
}

int ps()
{
  return 1;
}

int kill(int pid)
{
  return 1;
}

int my_mv(char *pathname)
{
  int ino1, ino2;
  MINODE *mip1, *mip2;
  char *src, *dst;
  
  src = strtok(pathname, " ");
  dst = strtok(0, "");
  
  ino1 = getino(src);
  if(ino1 == 0)
    {
      printf("Error, source doesn't exist\n");
      return -1;
    }

  ino2 = getino(dst);
  if(ino2 == 0)
    {
      printf("Error, destination doesn't exist\n");
      return -1;
    }

  mip1 = iget(globalDev, ino1);
  mip2 = iget(globalDev, ino2);

  if(mip1->dev == mip2->dev)
    {
      link_file(pathname);
      my_unlink(src);
    }
  if(mip1->dev != mip2->dev)
    {
      my_cp(src, dst);
      my_unlink(src);
    }
  
  return 1;
}

#endif
