/************* cd_ls_pwd.c file **************/

#ifndef __CDLSPWD_C__
#define __CDLSPWD_C__

#include "type.h"
#include "util.c"
//extern int dev;

char *t1 = "xwrxwrxwr-------";
char *t2 = "----------------";

int chdir(char *pathname)   
{
  MINODE *mip;
  if (!strcmp(pathname, "")) {
    mip = iget(globalDev, root->ino);
  }
  else {
    int ino = getino(pathname);
    if (ino == 0) {
      printf("SOMETHING WENT WRONG GETTING INO\n");
      return -1;
    }
  // return error if ino=0
    mip = iget(globalDev, ino);
  }


  if ((mip->INODE.i_mode & 0xF000) != 0x4000) {
    printf("%s IS NOT A DIR\n", pathname);
    return -1;
  }
// return error if not DIR
  iput(running->cwd);
// release old cwd
  running->cwd = mip;
// change cwd to mip
  return 1;
}

int ls_file(MINODE *mip, char *name)
{
    char ftime[64];
  int i; 

  // printf("%d\n\n", mip->INODE.i_mode);
  //defunct loop, will print out each individual bit of the permissions
  // for(int x = 16; x >= 0; x--) {
  //   printf("%d ", (mip->INODE.i_mode & (1 << x)));
  // }
  //printf("\n\n");
 if ((mip->INODE.i_mode & 0xF000) == 0x8000)  //Type file
   printf("%c",'-');
 if ((mip->INODE.i_mode & 0xF000) == 0x4000)  //Type dir
   printf("%c",'d');
 if ((mip->INODE.i_mode & 0xF000) == 0xA000)  //Type link
   printf("%c",'l');
 for (i=8; i >= 0; i--){
   if (mip->INODE.i_mode & (1 << i)) // print r|w|x
     printf("%c", t1[i]);
   else
     printf("%c", t2[i]);
   // or print -
 }
  
 printf("%4d ",mip->INODE.i_links_count); // link count
 printf("%4d ",mip->INODE.i_gid);         //print gid 
 printf("%4d ", mip->INODE.i_uid);        //print uid
 printf("%8d ", mip->INODE.i_size);       //print size
 time_t time = mip->INODE.i_mtime;        //print time
 strcpy(ftime, ctime(&time)); // print time in calendar form
 ftime[strlen(ftime)-1] = 0;
 // kill \n at end

 printf("%s ", ftime);
 // print name
  // print file basename
    printf("%s ", basename(name));
 // print -> linkname if symbolic file
 if ((mip->INODE.i_mode & 0xF000)== 0xA000){
    //use readlink() to read linkname
    
   char linkname[BLKSIZE];
   my_readlink(name, linkname);
   printf("-> %s", linkname); // print linked name
 }

  printf("[%d %d]", globalDev, mip->ino);
  printf("\n");
 return 1;
}

int ls_dir(MINODE *mip)
{
 char buf[BLKSIZE], temp[256];
  DIR *dp;
  char *cp;
  for (int i = 0; i < 13; i++) {
    if (mip->INODE.i_block[i] > 0) {
      get_block(globalDev, mip->INODE.i_block[i], buf);
      dp = (DIR *)buf;
      cp = buf;
      
      while (cp < buf + BLKSIZE){
        strncpy(temp, dp->name, dp->name_len);
        temp[dp->name_len] = 0;
          
        MINODE *mip_current = iget(globalDev, dp->inode);
        ls_file(mip_current, temp);

        iput(mip_current);

        cp += dp->rec_len;
        dp = (DIR *)cp;

      }
    }
  
  }
  printf("\n");
}

int my_ls(char *pathname)  
{
 if (!strcmp(pathname, "")) {
    ls_dir(running->cwd);
    return 1;
  }
  //tokenize(pathname);

  MINODE *mip_current = running->cwd;
  int ino = getino(pathname);
  mip_current = iget(globalDev, ino);

  if ((mip_current->INODE.i_mode & 0xF000) != 0x4000) {
      printf("%s IS NOT A DIR\n", pathname);
      return -1;
  }
  ls_dir(mip_current);
  return 1;
}

char *rpwd(MINODE *wd)
{

  if(wd == root)
    return 0;

  MINODE *pip;
  char name[MAX];
  u32 my_ino;
  int check, parent_ino = findino(wd, &my_ino);

  if(parent_ino == -1)
    return -1;

  pip = iget(globalDev, parent_ino); 
  check = findmyname(pip, my_ino, name);

  if(check == -1)
    return -1;

  rpwd(pip);
  printf("/%s", name);
  return 0;
}

char *pwd(MINODE *wd)
{
  int i = -1;
  printf("cwd: ");
  if (wd == root){
    printf("/\n");
    return 0;
  }
  else{
    i = rpwd(wd);
    printf("\n");
    return i;
  }
}

#endif