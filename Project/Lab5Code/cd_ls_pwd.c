#include <unistd.h>
char *t1 = "xwrxwrxwr-------";
char *t2 = "----------------";

extern char gpath[128];
extern char *name[64];
extern int n;

extern int tokenize(char *pathname);

/************* cd_ls_pwd.c file **************/
int cd(char* pathname)
{
  MINODE *mip;
  if (!strcmp(pathname, "")) {
    mip = iget(dev, root->ino);
  }
  else {
    int ino = getino(pathname);
    if (ino == 0) {
      printf("SOMETHING WENT WRONG GETTING INO\n");
      return -1;
    }
  // return error if ino=0
    mip = iget(dev, ino);
  }


  if ((mip->INODE.i_mode & 0xF000) != 0x4000) {
    printf("%s IS NOT A DIR\n", pathname);
    return -1;
  }
// return error if not DIR
  iput(running->cwd);
// release old cwd
  running->cwd = mip;
// change cwd to mip
}

int ls_file(MINODE *mip, char *name)
{
  char ftime[64];
  int i; 

  //defunct loop, will print out each individual bit of the permissions
  // for(int x = 16; x >= 0; x--) {
  //   printf("%d ", (mip->INODE.i_mode & (1 << x)));
  // }
  //printf("\n\n");
 if ((mip->INODE.i_mode & 0xF000) == 0x8000)  //Type file
   printf("%c",'-');
 if ((mip->INODE.i_mode & 0xF000) == 0x4000)  //Type dir
   printf("%c",'d');
 if ((mip->INODE.i_mode & 0xF000) == 0xA000)  //Type link
   printf("%c",'l');
 for (i=8; i >= 0; i--){
   if (mip->INODE.i_mode & (1 << i)) // print r|w|x
     printf("%c", t1[i]);
   else
     printf("%c", t2[i]);
   // or print -
 }
  
 printf("%4d ",mip->INODE.i_links_count); // link count
 printf("%4d ",mip->INODE.i_gid);         //print gid 
 printf("%4d ", mip->INODE.i_uid);        //print uid
 printf("%8d ", mip->INODE.i_size);       //print size
 time_t time = mip->INODE.i_mtime;        //print time
 strcpy(ftime, ctime(&time)); // print time in calendar form
 ftime[strlen(ftime)-1] = 0;
 // kill \n at end

 printf("%s ", ftime);
 // print name
 printf("%s ", basename(name)); // print file basename
 // print -> linkname if symbolic file
 if ((mip->INODE.i_mode & 0xF000)== 0xA000){
    //use readlink() to read linkname
    
   char linkname[64];
   readlink(name, linkname, 64);
   printf(" -> %s", linkname); // print linked name
 }

 printf("[%d %d]", dev, mip->ino);
 printf("\n");
}


int ls_dir(MINODE *mip)
{
  char buf[BLKSIZE], temp[256];
  DIR *dp;
  char *cp;

  get_block(dev, mip->INODE.i_block[0], buf);
  dp = (DIR *)buf;
  cp = buf;
  
  while (cp < buf + BLKSIZE){
     strncpy(temp, dp->name, dp->name_len);
     temp[dp->name_len] = 0;
      
     MINODE *mip_current = iget(dev, dp->inode);
     ls_file(mip_current, temp);

     //For future use    
     // iput(mip_current);

     cp += dp->rec_len;
     dp = (DIR *)cp;
  }
  printf("\n");
}

int ls(char* pathname)
{
  if (!strcmp(pathname, "")) {
    ls_dir(running->cwd);
    return 1;
  }
  //tokenize(pathname);

  MINODE *mip_current = running->cwd;
  int ino = getino(pathname);
  mip_current = iget(dev, ino);

  if ((mip_current->INODE.i_mode & 0xF000) != 0x4000) {
      printf("%s IS NOT A DIR\n", pathname);
      return -1;
  }
  ls_dir(mip_current);
}

char *pwd(MINODE *wd)
{
  int i = -1;
  printf("cwd: ");
  if (wd == root){
    printf("/\n");
    return 0;
  }
  else{
    i = rpwd(wd);
    printf("\n");
    return i;
  }
}
