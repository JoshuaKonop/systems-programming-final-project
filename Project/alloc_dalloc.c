#ifndef __ALLOC_DALLOC_C__
#define __ALLOC_DALLOC_C__

#include "type.h"

int ialloc(int dev)
{
  int i;
  char buf[BLKSIZE];
  //use imap, ninodes in mount table of dev
  MTABLE *mp = (MTABLE *)get_mtable(dev);
  printf("Got mtable mp\n");
  get_block(dev, mp->imap, buf);
  printf("Load imap into buffer\n");
  for (i = 0; i < mp->ninodes; i++) {
    //If an inode spot in imap is empty
    if (tst_bit(buf, i) == 0) {
      //set it to full
      printf("bit empty, assigning ino here\n");
      set_bit(buf, i);
      put_block(dev, mp->imap, buf);
      decFreeInodes(dev);
      return (i+1); //return ino of new inode
    }
    
  }
  return 0; //Out of INODES
}

int balloc(int dev)
{
  int i;
  char buf[BLKSIZE];
  //use bmap, nblocks in mount table of dev
  MTABLE *mp = (MTABLE *)get_mtable(dev);
  get_block(dev, mp->bmap, buf);
  for (i = 0; i < mp->nblocks; i++) {
    //If an inode spot in bmap is empty
    if (tst_bit(buf, i) == 0) {
      //set it to full
      set_bit(buf, i);
      put_block(dev, mp->bmap, buf);
      decFreeBlocks(dev);
      return (i+1); //return bno of new block
    }
    
  }
  return 0; //Out of INODES
}

int idalloc(int dev, int ino)
{
  char buf[BLKSIZE];
  MTABLE *mp = (MTABLE *)get_mtable(dev);
  if(ino > mp->ninodes)
    return -1;

  //get inode bitmap block
  get_block(dev, mp->imap, buf);
  clr_bit(buf, ino-1);
  //write buf back
  put_block(dev, mp->imap, buf);
  //update free inode count in SUPER and GD
  incFreeInodes(dev);
  return 1;
}

int bdalloc(int dev, int bno)
{
  char buf[BLKSIZE];
  MTABLE *mp = (MTABLE *)get_mtable(dev);
  if(bno > mp->nblocks)
    return -1;

  //get data_bloks bitmap block
  get_block(dev, mp->bmap, buf);
  clr_bit(buf, bno-1);
  //write buf back
  put_block(dev, mp->bmap, buf);
  //update free blocks count in SUPER and GD
  incFreeBlocks(dev);
  return 1;
}

MINODE *mialloc()
{
  // Code in Chapter 11.7.1

  return 0; // Eventually: Return entire MINODE *
}

int midalloc(MINODE *mip)
{
  // Code in Chapter 11.7.1

  return 1;
}

#endif
