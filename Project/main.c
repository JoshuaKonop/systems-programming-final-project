/****************************************************************************
*              YOUR_INITIALS testing ext2 file system                       *
*****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ext2fs/ext2_fs.h>
#include <string.h>
#include <libgen.h>
#include <sys/stat.h>
#include <time.h>

#include "type.h"
#include "cd_ls_pwd.c"
#include "symlink.c"
#include "write_cp.c"
#include "mount_umount.c"
#include "misc1.c"
#include "rmdir.c"
#include "link_unlink.c"

char gpath[128]; // global for tokenized components
char *pname[32];  // assume at most 32 components in pathname
int   n;         // number of component strings
char pathname[128];

int fd;

extern MINODE *iget();

int fs_init()
{
  int i,j;
  for (i=0; i<NMINODE; i++) // initialize all minodes as FREE
    minode[i].refCount = 0;

  for (i=0; i<NMTABLE; i++) // initialize mtables as FREE
    mtable[i].dev = 0;

  for (i=0; i<NOFT; i++) // initialize ofts as FREE
    oft[i].refCount = 0;

  for (i=0; i<NPROC; i++){ // initialize PROCs

    proc[i].status = READY; // ready to run

    proc[i].pid = i; // pid = 0 to NPROC-1

    proc[i].uid = i; // P0 is a superuser process

    for (j=0; j<NFD; j++)
      proc[i].fd[j] = 0; // all file descriptors are NULL
  
    proc[i].next = &proc[i+1]; // link list
  }

  proc[NPROC-1].next = &proc[0]; // circular list

  running = &proc[0]; // P0 runs first
}

int mount_root(char *rootdev)
{
  int i;
  MTABLE *mp;
  SUPER *sp;
  GD *gp;
  char buf[BLKSIZE];

  globalDev = open(rootdev, O_RDWR);

  if (globalDev < 0){
    printf("panic : can’t open root device\n");
    exit(1);
  }

  /* get super block of rootdev */
  get_block(globalDev, 1, buf);
  sp = (SUPER *)buf;

  /* check magic number */
  if (sp->s_magic != SUPER_MAGIC){
    printf("super magic=%x : %s is not an EXT2 filesys\n",
    sp->s_magic, rootdev);
    exit(0);
  }

  // fill mount table mtable[0] with rootdev information
  mp = &mtable[0]; // use mtable[0]
  mp->dev = globalDev;

  // copy super block info into mtable[0]
  ninodes = mp->ninodes = sp->s_inodes_count;
  nblocks = mp->nblocks = sp->s_blocks_count;
  strcpy(mp->devName, rootdev);
  strcpy(mp->mntName, "/");
  get_block(globalDev, 2, buf);
  gp = (GD *)buf;
  bmap = mp->bmap = gp->bg_block_bitmap;
  imap = mp->imap = gp->bg_inode_bitmap;
  iblock = mp->iblock = gp->bg_inode_table;
  printf("bmap=%d imap=%d iblock=%d\n", bmap, imap, iblock);

  // call iget(), which inc minode’s refCount
  
  root = iget(globalDev, 2); // get root inode
  mp->mntDirPtr = root; // double link
  root->mptr = mp;

  // set proc CWDs
  for (i=0; i<NPROC; i++) // set proc’s CWD
    proc[i].cwd = iget(globalDev, 2); // each inc refCount by 1

  printf("mount : %s mounted on / \n", rootdev);
  return 0;
}

int quit()
{
  int i;

  for (i=0; i<NMINODE; i++)
  {
    MINODE *mip = &minode[i];
    if (mip->refCount && mip->dirty)
    {
      mip->refCount = 1;
      iput(mip);
    }
  }

  exit(0);
}

int main(int argc, char *argv[ ])
{
  int ino, linefd, linep, lineposition, linenbytes, lineuid, mode;
  char buf[BLKSIZE];
  char line[128], cmd[32];

  char pathname2[128];

  char diskname[128];

  char *disk = rootdev;

  if (argc > 1)
  {
    disk = argv[1];

    rootdev = disk;
  }

  fs_init();

  mount_root(rootdev);

  while(1){
    printf("P%d running: ", running->pid);
    printf("input command : ");
    fgets(line, 128, stdin);
    line[strlen(line)-1] = 0;
    //Reset cmd and pathname
    strcpy(cmd, ""); strcpy(pathname, "");


    if (line[0]==0)
      continue;

    sscanf(line, "%s %s", cmd, pathname);
    
    if (!strcmp(cmd, "ls"))
      my_ls(pathname);
    if (!strcmp(cmd, "cd"))
      chdir(pathname);
    if (!strcmp(cmd, "pwd"))
      pwd(running->cwd);
    if (!strcmp(cmd, "quit"))
      quit();
    if (!strcmp(cmd, "mkdir"))
      make_dir(pathname);
    if (!strcmp(cmd, "creat"))
      creat_file(pathname);
    if (!strcmp(cmd, "rmdir"))
      remove_dir(pathname);
    if (!strcmp(cmd, "stat")) {
      struct stat x;
      x = my_stat(pathname);
      printf("ino=%d\nsize=%d\nuid=%d\n", x.st_ino, x.st_size, x.st_uid);
      //What else should I do here? print?
    }
    if (!strcmp(cmd, "utime")) {
      my_utime(pathname);
    }
    if (!strcmp(cmd, "chown")) {
        int uid; char x[32];
        printf("path=%s\n", line);
        sscanf(line, "%s %s %s", cmd, x, pathname);
        printf("path=%s\nid=%s",pathname,x);
        uid = atoi(x);
        printf("path=%s\nid=%x",pathname,uid);
        my_chown(pathname, uid);
    }
    if (!strcmp(cmd, "chmod")) {
        unsigned int mode; char x[32];
        sscanf(line, "%s %s %s", cmd, x, pathname);
        mode = atoi(x);
        my_chmod(pathname, mode);
    }
    if (!strcmp(cmd, "link")) {
        char x[128];
        sscanf(line, "%s %s %s", cmd, x, pathname);
        strcat(x, " ");
        strcat(x, pathname);
        link_file(x);
    }
    if (!strcmp(cmd, "unlink")) {
      my_unlink(pathname);
    }
    if (!strcmp(cmd, "symlink")) {
        char x[128];
        sscanf(line, "%s %s %s", cmd, x, pathname);
        strcat(x, " ");
        strcat(x, pathname);
        symlink_file(x);
    }
    if (!strcmp(cmd, "open")) {
        char x[128];
        sscanf(line, "%s %s %s", cmd, pathname, x);
        open_file(pathname, atoi(x));
    }
    if (!strcmp(cmd, "pfd")) {
        pfd();
    }
    if (!strcmp(cmd, "close")) {
        my_close(atoi(pathname));
    }
    if (!strcmp(cmd, "read")) {
        char x[128];
        sscanf(line, "%s %s %s", cmd, pathname, x);
        read_file(atoi(pathname), buf, atoi(x));
    }
    if (!strcmp(cmd, "cp")) {
        char x[128];
        sscanf(line, "%s %s %s", cmd, pathname, x);
        my_cp(pathname, x);
    }
    if (!strcmp(cmd, "write")) {
        char x[128];
        sscanf(line, "%s %s %s", cmd, pathname, x);
        write_file(atoi(pathname), x);
    }
    if (!strcmp(cmd, "cat")) {
        cat_file(pathname);
    }
    if (!strcmp(cmd, "sudo")) {
        sudo();
    }
    if (!strcmp(cmd, "menu")) {
        my_menu(pathname);
    }
  }
}
