/************* mkdir_creat.c file **************/

#ifndef __MKDIRCREAT_C__
#define __MKDIRCREAT_C__

#include "type.h"
#include "util.c"
#include "alloc_dalloc.c"

//Enter new child into parent
int enter_name(MINODE *pip, int ino, char *dirToMake) 
{   
    char buf[BLKSIZE];
    int i = 0;
    int remain, need_length;
    //For each data block of parent DIR
    for (; i < 12; i++) {
        printf("Checking iblock %d\n", i);
        if (pip->INODE.i_block[i] != 0) {
            get_block(globalDev, pip->INODE.i_block[i], buf);  
            printf("got parent i_block %d\n", pip->INODE.i_block[i]);
            
            //Get parents data block into a buf[];
            dp = (DIR *)buf;
            char *cp = buf;

            //step to final entry
            while (cp + dp->rec_len < buf + BLKSIZE) {
                cp += dp->rec_len;
                dp = (DIR *)cp;
                printf("dp name %s ", dp->name);
            }
            printf("\n");
            int ideal_length = 4 * ((8 + dp->name_len + 3)/4);            //What is name_len in this context??
            
            int rec_len = dp->rec_len - ideal_length; //except last entry
            //How to change last entry to be ideal_len + remaining BLKSIZE?

            //n_len is not name_len
            need_length = 4 * ((8 + strlen(dirToMake) + 3)/4);  //This should be correct

            printf("Calculate ideal_len, rec_len, need_len\nideal_len=%d\nrec_len=%d\nneed_len=%d\n", ideal_length, rec_len, need_length); 

            remain = dp->rec_len - ideal_length;
            printf("%d space remaining in current block\n", remain);

            if (remain >= need_length) {
                //enter new entry as LAST entry and trim previous
                dp->rec_len = ideal_length;   //trim
                cp += dp->rec_len;
                dp = (DIR *)cp;

                dp->inode = ino;
                dp->name_len = strlen(dirToMake);
                strncpy(dp->name, dirToMake, strlen(dirToMake));
                dp->rec_len = remain;
               // dp->file_type = 'd';
                printf("dp name %s \n%d\n%d\n%c\n", dp->name, dp->name_len, dp->rec_len, dp->file_type);
                printf("Enter new entry as last entry\n");
                printf("Put block back in memory\n");
                put_block(globalDev, pip->INODE.i_block[i], buf);   //hopefully this is right?
                break;
            }
            else {
                continue;
            }
           
        }
        else if (pip->INODE.i_block[i] <= 0) {
            int blk = balloc(globalDev);
            pip->INODE.i_block[i] = blk;
            get_block(globalDev, blk, buf);
            bzero(buf, BLKSIZE);
            dp = (DIR *)buf;
            char *cp = buf;
            int ideal_length = 4 * ((8 + dp->name_len + 3)/4);    
            
            need_length = 4 * ((8 + strlen(dirToMake) + 3)/4);  //This should be correct

            printf("Calculate ideal_len, rec_len, need_len\nideal_len=%d\nrec_len=%d\nneed_len=%d\n", ideal_length, 0, need_length); 

            dp->inode = ino;
            dp->name_len = strlen(dirToMake);
            strncpy(dp->name, dirToMake, strlen(dirToMake));
            dp->rec_len = BLKSIZE;
            // dp->file_type = 'd';
            printf("dp name %s \n%d\n%d\n", dp->name, dp->name_len, dp->rec_len);
            printf("Enter new entry as last entry\n");
            printf("Put block back in memory\n");
            put_block(globalDev, pip->INODE.i_block[i], buf);   //hopefully this is right?
            break;
        }
    }
    return 0;
}

int my_mkdir(MINODE *pip, char *dirToMake)
{
    //Allocate INODE and disk block
    int ino = ialloc(globalDev);
    int blk = balloc(globalDev);
    printf("New ino %d and blk %d allocated!\n", ino, blk);

    //load inode into a minode
    MINODE* mip = iget(globalDev, ino);
    printf("ino %d loaded into MINODE\n", ino);
    //init new inode as DIR type. 
    mip->INODE.i_mode = 16877;
    mip->INODE.i_size = BLKSIZE;
    mip->INODE.i_uid = running->uid;
    mip->INODE.i_gid = running->gid;
    mip->INODE.i_links_count = 2;
    mip->INODE.i_ctime = (unsigned int)time(0L);
    mip->INODE.i_mtime = (unsigned int)time(0L);
    mip->INODE.i_atime = (unsigned int)time(0L);

    printf("ino is initialized as type DIR\n");
    mip->INODE.i_block[0] = blk;    //Set iblock[0] to blk, others to 0
    for (int i =  1; i < 13; i++) { //there are 15 iblocks, right?
        mip->INODE.i_block[i] = 0;
    }
    mip->dirty = 1; //set dirty
    iput(mip);      //iput

    //make data block 0 of INODE contain . and ..
    char buf[BLKSIZE];
    bzero(buf, BLKSIZE);
    
    //Get dir dp
    DIR *dp = (DIR *)buf;
    //make . entry
    dp->inode = ino;
    printf("PARENT INO: %d", dp->inode);
    dp->rec_len = 12;
    dp->name_len = 1;
    dp->name[0] = '.';

    char* cp = buf + dp->rec_len;
    dp = (DIR *)cp;

    //make .. entry
    dp->inode = pip->ino;
    dp->rec_len = BLKSIZE - 12;
    dp->name_len = 2;
    dp->name[0] = dp->name[1] = '.';
    printf("Write blk to disk\n");
    put_block(globalDev, blk, buf);

    printf("Enter child into parent\n");
    enter_name(pip, ino, dirToMake);
    return 1;
}

int make_dir(char * pathname)
{
    //Split into path and cmd
    tokenize(pathname);
    char path[128], dirToMake[32];
    strcpy(path, ""); strcpy(dirToMake, "");

    for(int x = 0; x < n-1; x++) {
        printf("Current name: %s\n", name[x]);
        strcat(path, name[x]);
    }

    strcpy(dirToMake, name[n - 1]);
    printf("path=%s\ndir=%s\n",path, dirToMake);

    //Get MINODE of parent path
    //Look into dirname function
    MINODE* pmip;
    int pino;
    if (!strcmp(path, "")) {
        pino = running->cwd->ino;
        pmip = iget(globalDev, pino);
        char buf[256];
        if (accessINO(pino, 'r') == 0 || accessINO(pino, 'w') == 0 || accessINO(pino, 'x') == 0) {
            return -1;
        }
    }
    else {
        pino = getino(path);
        if (pino <= 0) {
            //add error message
            return -1;
        }
        pmip = iget(globalDev, pino);
        if (access(path, 'r') == 0 || access(path, 'w') == 0 || access(path, 'x') == 0) {
            return -1;
        }
    }
    printf("ino of parent: %d", pino);

    //Make sure parent is of type DIR
    if ((pmip->INODE.i_mode & 0xF000) != 0x4000) {
      printf("%s IS NOT A DIR\n", path);
      return -1;
    }

    //Check to see if parent contains dirToMake already
    if (search(pmip, dirToMake) != 0) {
        printf("%s ALREADY EXISTS IN %s\n", dirToMake, path);
        return -1;
    }  

    my_mkdir(pmip, dirToMake);
    
    //Once the rest works, this will need to work
    pmip->INODE.i_links_count++;
    pmip->dirty = 1;
    iput(pmip);
    return 1;
}

int my_creat(MINODE *pip, char *dirToMake)
{
       //Allocate INODE and disk block
    int ino = ialloc(globalDev);
    int blk = balloc(globalDev);
    printf("New ino %d and blk %d allocated!\n", ino, blk);

    //load inode into a minode
    MINODE* mip = iget(globalDev, ino);
    printf("ino %d loaded into MINODE\n", ino);
    //init new inode as FILE type. 
    mip->INODE.i_mode = 33188;
    mip->INODE.i_size = 0;
    mip->INODE.i_uid = running->uid;
    mip->INODE.i_gid = running->gid;
    mip->INODE.i_links_count = 1;
    mip->INODE.i_ctime = (unsigned int)time(0L);
    mip->INODE.i_mtime = (unsigned int)time(0L);
    mip->INODE.i_atime = (unsigned int)time(0L);

    printf("ino is initialized as type DIR\n");
    mip->INODE.i_block[0] = blk;    //Set iblock[0] to blk, others to 0
    for (int i =  1; i < 13; i++) { //there are 15 iblocks, right?
        mip->INODE.i_block[i] = 0;
    }
    mip->dirty = 1; //set dirty
    iput(mip);      //iput

    //make data block 0 of INODE contain . and ..
    char buf[BLKSIZE];
    bzero(buf, BLKSIZE);
    
    //Get dir dp
    // DIR *dp = (DIR *)buf;
    // //make . entry
    // dp->inode = ino;
    // printf("PARENT INO: %d", dp->inode);
    // dp->rec_len = 12;
    // dp->name_len = 1;
    // dp->name[0] = '.';

    // char* cp = buf + dp->rec_len;
    // dp = (DIR *)cp;

    // //make .. entry
    // dp->inode = pip->ino;
    // dp->rec_len = BLKSIZE - 12;
    // dp->name_len = 2;
    // dp->name[0] = dp->name[1] = '.';
     printf("Write blk to disk\n");
    put_block(globalDev, blk, buf);

    printf("Enter child into parent\n");
    enter_name(pip, ino, dirToMake);
    return 1;
}

int creat_file(char *pathname)
{
    //Split into path and cmd
    tokenize(pathname);
    char path[128], dirToMake[32];
    strcpy(path, ""); strcpy(dirToMake, "");

    for(int x = 0; x < n-1; x++) {
        printf("Current name: %s\n", name[x]);
        strcat(path, name[x]);
    }

    strcpy(dirToMake, name[n - 1]);
    printf("path=%s\ndir=%s\n",path, dirToMake);

    //Get MINODE of parent path
    MINODE* pmip;
    int pino;
    if (!strcmp(path, "")) {
        pino = running->cwd->ino;
        pmip = iget(globalDev, pino);
        char buf[256];
        if (accessINO(pino, 'r') == 0 || accessINO(pino, 'w') == 0 || accessINO(pino, 'x') == 0) {
            return -1;
        }
    }
    else {
        pino = getino(path);
        if (pino <= 0) {
            //add error message
            return -1;
        }
        pmip = iget(globalDev, pino);
        if (access(path, 'r') == 0 || access(path, 'w') == 0 || access(path, 'x') == 0) {
            return -1;
        }
    }
    printf("ino of parent: %d", pino);

    //Make sure parent is of type DIR
    if ((pmip->INODE.i_mode & 0xF000) != 0x4000) {
      printf("%s IS NOT A DIR\n", path);
      return -1;
    }

    //Check to see if parent contains dirToMake already
    if (search(pmip, dirToMake) != 0) {
        printf("%s ALREADY EXISTS IN %s\n", dirToMake, path);
    }  

    my_creat(pmip, dirToMake);
    
    //Once the rest works, this will need to work
    //pmip->INODE.i_links_count++;
    pmip->dirty = 1;
    iput(pmip);
    return 1;
}

#endif