/************* write_cp.c file **************/

#ifndef __WRITECP_C__
#define __WRITECP_C__

#include "read_cat.c"
#include "link_unlink.c"

int write_file(int fd, char *buf)
{
  char temp[128];
  int i, length, nbytes;

  if(running->fd[fd] == 0)
    {
      printf("Error, file is not open\n");
      return -1;
    }
  if(running->fd[fd]->mode == 0)
    {
      printf("Error, file is not open for WR, RW, or APPEND\n");
      return -1;
    }
 // printf("size of buf=%d\nbuftype size=%d\n", count, sizeof(buf[0]));

  length = strlen(buf) / sizeof(buf[0]); //arr len = size of arr / size of arr's data type
  printf("size of insert=%d\nbuf=%s\n",length, buf);
  strncpy(temp, buf, length);
  temp[length] = 0; // make temp a string
  nbytes = strlen(temp);

  i = my_write(fd, temp, nbytes);
  return i; // Eventually: return the results of my_write
}

int my_write(int fd, char *buf, int nbytes)
{
  int count = 0, blk, lbk, startByte, remain;
  char wbuf[BLKSIZE], *cq = buf;
  int in_buf[256], d_in_buf[256];
  OFT *oft = running->fd[fd];
  MINODE* mip = oft->minodeptr;

  while(nbytes > 0)
    {
      lbk = oft->offset / BLKSIZE;
      startByte = oft->offset % BLKSIZE;

      if(lbk < 12) //direct
	{
	  if(mip->INODE.i_block[lbk] == 0)
	    mip->INODE.i_block[lbk] = balloc(mip->dev);
	  
	  blk = mip->INODE.i_block[lbk];
	}
      else if(lbk >= 12 && lbk < 12 + 256) //indirect
	{
	  if(mip->INODE.i_block[12] == 0)
	    {
	      mip->INODE.i_block[12] = balloc(mip->dev);
	      
	      get_block(mip->dev, mip->INODE.i_block[12], (char*)in_buf);
	      bzero(in_buf, BLKSIZE);

	      put_block(mip->dev, mip->INODE.i_block[12], (char*)in_buf);
	    }
	  
	  get_block(mip->dev, mip->INODE.i_block[12], (char*)in_buf);
	  blk = in_buf[lbk-12];
	  
	  if(blk == 0)
	    {
	      in_buf[lbk-12] = balloc(mip->dev);
	      blk = in_buf[lbk-12];
	      put_block(mip->dev, mip->INODE.i_block[12], (char*)in_buf);
	    }
	}
  else if (lbk >= 12 + 256) {

    if(mip->INODE.i_block[13] == 0)
	    {
	      mip->INODE.i_block[13] = balloc(mip->dev);
	      
	      get_block(mip->dev, mip->INODE.i_block[13], (char*)in_buf);
	      bzero(in_buf, BLKSIZE);

	      put_block(mip->dev, mip->INODE.i_block[13], (char*)in_buf);
	    }
    printf("IN DOUBLE INDIRECT\n");
	  get_block(mip->dev, mip->INODE.i_block[13], (char*)in_buf);
    int i = (lbk - 12 - 256) / 256;
    int j = (lbk - 12 - 256) % 256;
  
    if (in_buf[i] == 0)  {
      in_buf[i] = balloc(mip->dev);
	    blk = in_buf[i];

	    put_block(mip->dev, mip->INODE.i_block[13], (char*)in_buf);
    }
  
    get_block(mip->dev, in_buf[i], (char*)d_in_buf);
	  blk = d_in_buf[j];
	  
	  if(blk == 0)
	  {
      d_in_buf[j] = balloc(mip->dev);
	    blk = d_in_buf[j];
	    put_block(mip->dev, in_buf[i], (char*)d_in_buf);
	  }
  }

      get_block(mip->dev, blk, wbuf);
      char *cp = wbuf + startByte;
      remain = BLKSIZE - startByte;

      while(remain > 0)
	{
	  *cp++ = *cq++;
	  nbytes--; remain--;
	  oft->offset++;
          count++;
	  if(oft->offset > mip->INODE.i_size)
	    mip->INODE.i_size++;
	  if(nbytes <= 0)
	    break;
	}
      put_block(mip->dev, blk, wbuf);
    }
  mip->dirty = 1;
  printf("LBLK IS %d\n BLK IS %d\n", lbk, blk);
  // if (lbk < 1|| blk < 1) {
  //     printf("SOMETHING GONE VERY WRONG\n");
  // }
  printf("wrote %d char into file descriptor %d\n", count, fd);
  return count; // Eventually: return the number of bytes written
}

int my_cp(char *pathname, char* dirToMake)
{
    int fd = open_file(pathname, 0);
    int gd = open_file(dirToMake, 1);
    char buftemp[BLKSIZE];
    char *buf = &buftemp;
    while (n= read_file(fd, buf, BLKSIZE)) {
      printf("N IS NOW EQUAL TO %d\n", n);
        if (my_write(gd, buf, n) < 1) {
          printf("THERES SOMETHING WRONG HERE");
          my_close(fd); my_close(gd);
          return -1;
        }
        bzero(buf, BLKSIZE);
    }
    my_close(fd); my_close(gd);
    return 1;
}

#endif
