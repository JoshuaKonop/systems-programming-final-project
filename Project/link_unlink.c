/************* link_unlink.c file **************/

#ifndef __LINKUNLINK_C__
#define __LINKUNLINK_C__

#include "mkdir_creat.c"
#include "rmdir.c"
#include "cd_ls_pwd.c"

int my_link(MINODE *filePip, char *fileName, int fileIno, MINODE *linkPip, char *linkName)
{



}

int link_file(char *pathname)
{
    char *oldfile, *newfile;
    printf("Original pathname=%s\n", pathname);

    oldfile = strtok(pathname, " ");
    newfile = strtok(0, "");

    printf("Got oldfile=%s, newfile=%s\n", oldfile, newfile);

    int oino = getino(oldfile);
    MINODE* omip = iget(globalDev, oino);
    printf("Get old ino=%d", oino);

    if (access(oldfile, 'r') == 0 || access(oldfile, 'w') == 0 || access(oldfile, 'x') == 0 || access(newfile, 'r') == 0 || access(newfile, 'w') == 0 || access(newfile, 'x') == 0) {
      return -1;
    }

    if (oino <= 0 || (omip->INODE.i_mode & 0xF000) == 0x4000) {
        printf("OLD FILE IS DIR TYPE OR DOES NOT EXIST...\n");
        return -1;
    }
    if (getino(newfile) != 0) {
        printf("NEW FILE ALREADY EXISTS IN CWD...\n");
        return -1;
    }
    char* parent = dirname(newfile), *child = basename(newfile);
    printf("Collect parent=%s, child=%s\n", parent, child);

    int pino = getino(parent);
    MINODE *pmip = iget(globalDev, pino);
    printf("Get parent ino=%d", pino);

    enter_name(pmip, oino, child);
    omip->INODE.i_links_count++;
    omip->dirty = 1;
    iput(omip);
    iput(pmip);

    return 1;
}

int my_unlink(char *pathname)
{
  int ino, pino, i;
  MINODE *mip, *pmip;
  char parent[MAX], child[MAX], temp[MAX];

  if(!strcmp(pathname, "") || !strcmp(pathname, "..") || !strcmp(pathname, "."))
    {
      printf("Invalid Input\n");
      return -1;
    }

  ino = getino(pathname);
  mip = iget(globalDev, ino);

  if (mip->INODE.i_uid != running->uid && running->uid != 0) {
    printf("Not file owner!\n");
    return -1;
  }

  if((mip->INODE.i_mode & 0xF000) == 0x4000) // if dir
    {
      printf("Error, %s is a directory\n", pathname);
      iput(mip);
      return -1;
    }

  strcpy(temp, pathname);
  strcpy(parent, dirname(temp));
  strcpy(temp, pathname);
  strcpy(child, basename(temp));

  pino = getino(parent);
  pmip = iget(globalDev, pino);
  rm_child(pmip, child);
  pmip->dirty=1;
  iput(pmip);
  
  mip->INODE.i_links_count--;
  if(mip->INODE.i_links_count > 0)
    {
      mip->dirty=1;
    }
  else if(mip->INODE.i_links_count == 0)
    {
      truncate(mip);
  //     for(i=0;i<12;i++)
	// {
	//   bdalloc(mip->dev, mip->INODE.i_block[i]);
	//   mip->INODE.i_block[i] = 0;
	// }

      idalloc(mip->dev, mip->ino);
    }

  iput(mip);
  return 1;
}

#endif
