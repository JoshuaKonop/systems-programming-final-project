/*********** util.c file ****************/

#ifndef __UTIL_C__
#define __UTIL_C__

#include "type.h"

int get_block(int dev, int blk, char *buf)
{
  lseek(dev, (long)blk*BLKSIZE, 0);
  read(dev, buf, BLKSIZE);
  return 1;
}

int put_block(int dev, int blk, char *buf)
{
  lseek(dev, (long)blk*BLKSIZE, 0);
  write(dev, buf, BLKSIZE);
  return 1;
}

int tst_bit(char *buf, int bit) // pg. 333
{
  return buf[bit / 8] & (1 << (bit % 8)); 
}

int set_bit(char *buf, int bit) // pg. 333
{
  buf[bit/8] |= (1 << (bit % 8));
  return 1;
}

int clr_bit(char *buf, int bit) // pg. 306
{
  buf[bit/8] &= ~(1 << (bit%8));
  return 1;
}

MTABLE * get_mtable(int dev)
{
  //NEED TO FIX THIS? maybe. It could be correct. WORKS FOR NOW.
  return &mtable;
}

int incFreeInodes(int dev)
{
  char buf[BLKSIZE];
  SUPER *sp;
  GD *gp;

  //inc free inodes count in super and GD
  get_block(dev, 1, buf);
  sp = (SUPER *)buf;
  sp->s_free_inodes_count++;
  put_block(dev, 1, buf);

  get_block(dev, 2, buf);
  gp = (GD *)buf;
  gp->bg_free_inodes_count++;
  put_block(dev, 2, buf);
  return 1;
}

int incFreeBlocks(int dev)
{	
  char buf[BLKSIZE];
  SUPER *sp;
  GD *gp;

  //inc free blocks count in super and GD
  get_block(dev, 1, buf);
  sp = (SUPER *)buf;
  sp->s_free_blocks_count++;
  put_block(dev, 1, buf);
  
  get_block(dev, 2, buf);
  gp = (GD *)buf;
  gp->bg_free_blocks_count++;
  put_block(dev, 2, buf);
  return 1;
}

//Decrement free inodes count in SUPER and GD
int decFreeInodes(int dev)
{
  char buf[BLKSIZE];
  //get SUPER
  get_block(dev, 1, buf);
  sp = (SUPER *)buf;
  //Decrement SUPER
  sp->s_free_inodes_count--;
  put_block(dev, 1, buf);
  
  //get GD
  get_block(dev, 2, buf);
  gp = (GD *)buf;
  //Decrement GD
  gp->bg_free_inodes_count--;
  put_block(dev, 2, buf);
  return 1;
}

int decFreeBlocks(int dev)
{
  char buf[BLKSIZE];
  //get SUPER
  get_block(dev, 1, buf);
  sp = (SUPER *)buf;
  //Decrement SUPER
  sp->s_free_blocks_count--;
  put_block(dev, 1, buf);
  
  //get GD
  get_block(dev, 2, buf);
  gp = (GD *)buf;
  //Decrement GD
  gp->bg_free_blocks_count--;
  put_block(dev, 2, buf);
  return 1;
}

int tokenize(char *pathname)
{
  int i;
  char *s;
//  printf("tokenize %s\n", pathname);

  strcpy(gline, pathname);   // tokens are in global gpath[ ]
  n = 0;

  s = strtok(gline, "/");
  while(s){
    name[n] = s;
    n++;
    s = strtok(0, "/");
  }
  name[n] = 0;
  
  for (i= 0; i<n; i++)
 //   printf("%s  ", name[i]);
 // printf("\n");
  return n;
}

MINODE *iget(int dev, int ino)
{
  int i;
  MINODE *mip;
  char buf[BLKSIZE];
  int blk, offset;
  INODE *ip;

  for (i=0; i<NMINODE; i++){
    mip = &minode[i];
    if (mip->refCount && mip->dev == dev && mip->ino == ino){
       mip->refCount++;
       //printf("found [%d %d] as minode[%d] in core\n", dev, ino, i);
       return mip;
    }
  }
    
  for (i=0; i<NMINODE; i++){
    mip = &minode[i];
    if (mip->refCount == 0){
       //printf("allocating NEW minode[%d] for [%d %d]\n", i, dev, ino);
       mip->refCount = 1;
       mip->dev = dev;
       mip->ino = ino;

       // get INODE of ino into buf[ ]    
       blk    = (ino-1)/8 + iblock;
       offset = (ino-1) % 8;

       //printf("iget: ino=%d blk=%d offset=%d\n", ino, blk, offset);

       get_block(dev, blk, buf);    // buf[ ] contains this INODE
       ip = (INODE *)buf + offset;  // this INODE in buf[ ] 
       // copy INODE to mp->INODE
       mip->INODE = *ip;
       return mip;
    }
  }   
  printf("PANIC: no more free minodes\n");
  return 0;
}

void iput(MINODE *mip)
{
 int i, block, offset;
 char buf[BLKSIZE];
 INODE *ip;

 if (mip==0) 
     return;

 mip->refCount--;
 
 if (mip->refCount > 0) return;
 if (!mip->dirty)       return;
 
	/* write INODE back to disk */
 block = (mip->ino - 1) / 8 + iblock; // 8 inodes per block
 offset = (mip->ino - 1) % 8; //offset of INODE

 get_block(mip->dev, block, buf); // get block containing INODE
 ip = (INODE *)buf + offset; // ip->INODE 
 *ip = mip->INODE;
 put_block(mip->dev, block, buf);
 mip->refCount = 0;
}


int search(MINODE *mip, char *name)
{
   int i; 
   char *cp, c, sbuf[BLKSIZE], temp[256];
   DIR *dp;
   INODE *ip;

//   printf("search for %s in MINODE = [%d, %d]\n", name,mip->dev,mip->ino);
   ip = &(mip->INODE);

   /*** search for name in mip's data blocks: ASSUME i_block[0] ONLY ***/

   get_block(globalDev, ip->i_block[0], sbuf);
   dp = (DIR *)sbuf;
   cp = sbuf;
//   printf("  ino   rlen  nlen  name\n");

   while (cp < sbuf + BLKSIZE){
     strncpy(temp, dp->name, dp->name_len); // dp->name is NOT a string
     temp[dp->name_len] = 0;                // temp is a STRING
//     printf("%4d  %4d  %4d    %s\n", 
//	    dp->inode, dp->rec_len, dp->name_len, temp); // print temp !!!

     if (strcmp(temp, name)==0){            // compare name with temp !!!
//        printf("found %s : ino = %d\n", temp, dp->inode);
        return dp->inode;
     }

     cp += dp->rec_len;
     dp = (DIR *)cp;
   }
   return 0;
}

//For checking access on getino without actually having to use getino.
int getinoCheck(char *pathname)
{
  int i, ino, blk, offset;
  char buf[BLKSIZE];
  INODE *ip;
  MINODE *mip;

  //printf("getino: pathname=%s\n", pathname);
  if (strcmp(pathname, "/")==0)
      return 2;
  
  // starting mip = root OR CWD
  if (pathname[0]=='/')
     mip = root;
  else
     mip = running->cwd;

  mip->refCount++;         // because we iput(mip) later
  
  tokenize(pathname);

  for (i=0; i<n; i++){
  //    printf("===========================================\n");
  //    printf("getino: i=%d name[%d]=%s\n", i, i, name[i]);

      ino = search(mip, name[i]);

      if (ino==0){
         iput(mip);
  //       printf("name %s does not exist\n", name[i]);
         return 0;
      }

      iput(mip);
      mip = iget(globalDev, ino);
   }

   iput(mip);
   return ino;
}

int access(char *filename, char mode)  // mode = r|w|x:
{
  int r = 0;
 // return 1;
  short int ownR = 0x0100;
  short int ownW = 0x0080;
  short int ownX = 0x0040;
  short int othR = 0x0004;
  short int othW = 0x0002;
  short int othX = 0x0000;

  if (running->uid == 0)   // SUPERuser always OK
     return 1;

  // NOT SUPERuser: get file's INODE
  int ino = getinoCheck(filename);
  MINODE* mip = iget(globalDev, ino);
  char buf[BLKSIZE];

//  int check = 0;
  if (mip->INODE.i_uid == running->uid) {
    if (mode == 'r') {
        if (mip->INODE.i_mode & ownR == 1) {
          r = 1;
        }
    }
    else if (mode == 'w') {
        if (mip->INODE.i_mode & ownW == 1) {
          r = 1;
        }
    }
    else if (mode == 'x') {
        if (mip->INODE.i_mode & ownX == 1) {
          r = 1;
        }
    }
  }
  else {
    if (mode == 'r') {
        if (mip->INODE.i_mode & othR == 1) {
          r = 1;
        }
    }
    else if (mode == 'w') {
        if (mip->INODE.i_mode & othW == 1) {
          r = 1;
        }
    }
    else if (mode == 'x') {
        if (mip->INODE.i_mode & othX == 1) {
          r = 1;
        }
   }
  }
  iput(mip);
  
  if (r == 0) printf("Action not permitted!\n");

  return r;

}

int accessINO(int ino, char mode)  // mode = r|w|x:
{
  int r = 0;

  short int ownR = 0x0100;
  short int ownW = 0x0080;
  short int ownX = 0x0040;
  short int othR = 0x0004;
  short int othW = 0x0002;
  short int othX = 0x0000;

  if (running->uid == 0)   // SUPERuser always OK
     return 1;

  // NOT SUPERuser: get file's INODE
  MINODE* mip = iget(globalDev, ino);
  char buf[BLKSIZE];

//  int check = 0;
  if (mip->INODE.i_uid == running->uid) {
    if (mode == 'r') {
        if (mip->INODE.i_mode & ownR == 1) {
          r = 1;
        }
    }
    else if (mode == 'w') {
        if (mip->INODE.i_mode & ownW == 1) {
          r = 1;
        }
    }
    else if (mode == 'x') {
        if (mip->INODE.i_mode & ownX == 1) {
          r = 1;
        }
    }
  }
  else {
    if (mode == 'r') {
        if (mip->INODE.i_mode & othR == 1) {
          r = 1;
        }
    }
    else if (mode == 'w') {
        if (mip->INODE.i_mode & othW == 1) {
          r = 1;
        }
    }
    else if (mode == 'x') {
        if (mip->INODE.i_mode & othX == 1) {
          r = 1;
        }
   }
  }
  iput(mip);
  
  if (r == 0) printf("Action not permitted!\n");

  return r;

}

int getino(char *pathname)
{
  int i, ino, blk, offset;
  char buf[BLKSIZE];
  INODE *ip;
  MINODE *mip;

  //printf("getino: pathname=%s\n", pathname);
  if (strcmp(pathname, "/")==0)
      return 2;
  
  // starting mip = root OR CWD
  if (pathname[0]=='/')
     mip = root;
  else
     mip = running->cwd;

  mip->refCount++;         // because we iput(mip) later
  
  tokenize(pathname);

  for (i=0; i<n; i++){
  //    printf("===========================================\n");
  //    printf("getino: i=%d name[%d]=%s\n", i, i, name[i]);

      if (access(name[i], 'r') == 0 || access(name[i], 'x') == 0) {
        return -1;
      }
      ino = search(mip, name[i]);

      if (ino==0){
         iput(mip);
  //       printf("name %s does not exist\n", name[i]);
         return 0;
      }

      iput(mip);
      mip = iget(globalDev, ino);
   }

   iput(mip);
   return ino;
}

int findmyname(MINODE *parent, u32 myino, char *myname) 
{
  int i;
  char *cp, sbuf[BLKSIZE];
  DIR *dp;

  for(i=0;i<12;i++)
    {
      if(parent->INODE.i_block[i] == 0)
	         return -1;

   //Commented this out because otherwise I got a compilation error
      get_block(parent->dev, parent->INODE.i_block[i], sbuf);
      dp = (DIR *)sbuf;
      cp = sbuf;

      while (cp < sbuf + BLKSIZE){
        if (dp->inode == myino){
          //printf("found %s : ino = %d\n", dp->name, dp->inode);
          strncpy(myname, dp->name, dp->name_len);
          myname[dp->name_len] == 0;
          return 0;
        }
        cp += dp->rec_len;
        dp = (DIR *)cp;
      }
    }
  return -1;
}

int findino(MINODE *mip, u32 *myino) // myino = ino of . return ino of ..
{
  int dev, pip;
  u8 str[BLKSIZE];
  u8 *ptr;
  DIR *dp;
  INODE *ip;

  if(!mip)
    return -1;

  dev = mip->dev;
  ip = &mip->INODE;

  get_block(dev, ip->i_block[0], str);
  ptr = str;
  dp = (DIR *)str;
  *myino = dp->inode;
  
  ptr += dp->rec_len;
  dp =(DIR *)ptr;
  pip = dp->inode;
  return pip;
}

int sudo() {
  if (running->uid == 0) {
    printf("Setting user id to 100\n");
    running->uid = 100;
    running->gid = 100;
  }
  else if (running->uid == 100) {
    printf("Setting user id to 0\n");
    running->uid = 0;
    running->gid = 0;
  }
}

int my_menu(char* cmd) {

    if (!strcmp(cmd, "")) {
      printf("List of all commands:\n\nls\ncd\npwd\nquit\nmkdir\ncreat\nrmdir\nstat\nutime\nchown\nchmod\nlink\nunlink\nsymlink\nopen\npfd\nclose\ncp\ncat\nsudo\n\n");
      printf("Type 'menu <command-name>' to learn more about a specific command.\n");
    }
    if (!strcmp(cmd, "ls")) {
        printf("This command will list all the contents of the specificed directory, or the current directory if no directory is specified.\n");
        printf("\nUsage: 'ls <specified-directory' or 'ls'\n");
      }
    if (!strcmp(cmd, "cd")) {
        printf("This command change directory to the specified directory. If no directory is specified, then will change directory to ROOT.\n");
        printf("\nUsage: 'cd <specified-directory' or 'cd'\n");
      }
    if (!strcmp(cmd, "pwd")) {
        printf("This command will print the current working directory.\n");
        printf("\nUsage: 'pwd'\n");
      }
    if (!strcmp(cmd, "quit")) {
        printf("This command will exit the program.\n");
        printf("\nUsage: 'quit'\n");
      }
    if (!strcmp(cmd, "mkdir"))  {
        printf("This command will create a new directory, with a name specified by the user.\n");
        printf("\nUsage: 'mkdir <dir-name>'\n");
      }
    if (!strcmp(cmd, "creat"))  {
        printf("This command will create a new file, with a name specified by the user.\n");
        printf("\nUsage: 'creat <file-name>'\n");
      }
    if (!strcmp(cmd, "rmdir"))  {
        printf("This command will remove a specified directory.\n");
        printf("\nUsage: 'rmdir <dir-name>'\n");
      }
    if (!strcmp(cmd, "stat")) {
        printf("This command will create a stat structure that contains the file information of a file specified by the user.\n");
        printf("\nUsage: 'stat <file-name>'\n");
    }
    if (!strcmp(cmd, "utime")) {
        printf("This command will change the time accessed and modified on a specified file to the current time.\n");
        printf("\nUsage: 'utime <file-name>'\n");
    }
    if (!strcmp(cmd, "chown")) {
        printf("This command will change the user on a specified file to a specified user.\n");
        printf("\nUsage: 'chown <file-name> <user-id>'\n");
    }
    if (!strcmp(cmd, "chmod")) {
        printf("This command will change the mode on a specified file to a specified mode.\n");
        printf("\nUsage: 'chmod <file-name> <mode>'\n");
    }
    if (!strcmp(cmd, "link")) {
        printf("This command will create a new file, hard linked to an old file.\n");
        printf("\nUsage: 'link <old-file-name> <new-file-name>'\n");
    }
    if (!strcmp(cmd, "unlink")) {
        printf("This command will unlink a file, removing it if it is the only reference to that file.\n");
        printf("\nUsage: 'unlink <file-name>'\n");
    }
    if (!strcmp(cmd, "symlink")) {
        printf("This command will create a new link file and symbolically link it to an old file.\n");
        printf("\nUsage: 'symlink <old-file-name> <new-file-name>'\n");
    }
    if (!strcmp(cmd, "open")) {
        printf("This command will open a file with a set permission.\n");
        printf("\nUsage: 'open <file-name> <0, 1, 2, or 3 (0=R, 1=W, 2=RW, 3=APPEND)>'\n");
    }
    if (!strcmp(cmd, "pfd")) {
        printf("This command will print the information of all currently open files, or nothing if no files are open.\n");
        printf("\nUsage: 'pfd'\n");
    }
    if (!strcmp(cmd, "close")) {
        printf("This command will close an open file.\n");
        printf("\nUsage: 'close <file-descriptor (id #)>'\n");
    }
    if (!strcmp(cmd, "read")) {
        printf("This command will read n bytes from a file into a specified buffer. Should not be called here, this is for testing. Use cat to read contents of a file.\n");
        printf("\nUsage: 'read <file> <buffer> <number of characters to read>'\n");
    }
    if (!strcmp(cmd, "cp")) {
        printf("This command will copy the contents of a file and write them into a new file, creating the new file if it does not exist.\n");
        printf("\nUsage: 'cp <old-file> <new-file>'\n");
    }
    if (!strcmp(cmd, "write")) {
        printf("This command will write n bytes from a buffer into a specified file. Should not be called here, this is for testing. Use cp to write contents of a file into a new file.\n");
        printf("\nUsage: 'write <file> <number of characters to write>'\n");
    }
    if (!strcmp(cmd, "cat")) {
        printf("This command will print the contents of a file to the terminal.\n");
        printf("\nUsage: 'cat <file>'\n");
    }
    if (!strcmp(cmd, "sudo")) {
        printf("This command will change the current user to 100 (non-sudo) if user is 0, or 0 (sudo) if user is 100.\n");
        printf("\nUsage: 'sudo'\n");
    }
}

#endif
