/************* symlink.c file **************/

#ifndef __SYMLINK_C__
#define __SYMLINK_C__

#include "mkdir_creat.c"
#include "rmdir.c"

int symlink_file(char *pathname)
{
  char *oldfile, *newfile;
  printf("Original pathname=%s\n", pathname);

  oldfile = strtok(pathname, " ");
  newfile = strtok(0, "");

  printf("Got oldfile=%s, newfile=%s\n", oldfile, newfile);

  int oino = getino(oldfile);
  MINODE* omip = iget(globalDev, oino);
  printf("Get old ino=%d", oino);

  if (oino <= 0) {
      printf("OLD FILE DOES NOT EXIST...\n");
      return -1;
  }
  if (getino(newfile) != 0) {
      printf("NEW FILE ALREADY EXISTS IN CWD...\n");
      return -1;
  }
  char* parent = dirname(newfile), *child = basename(newfile);
  printf("Collect parent=%s, child=%s\n", parent, child);

  int pino = getino(parent);
  printf("Found path ino=%d\n", pino);
  MINODE* pmip = iget(globalDev, pino);
  my_creat(pmip, child);
  printf("Created child symlink\n");

  int cino = getino(child);
  printf("Get link ino=%d\n", cino);
  MINODE* cmip = iget(globalDev, cino);

  cmip->INODE.i_mode = 0xA1ED;//LNK; 
  cmip->INODE.i_size = strlen(oldfile);
  cmip->INODE.i_links_count = 1;
  printf("Symlink updated\n");

  char buf[BLKSIZE];
  get_block(globalDev, cmip->INODE.i_block[0], buf);

  strncpy(buf, oldfile, strlen(oldfile));

  put_block(globalDev, cmip->INODE.i_block[0], buf);

  cmip->dirty = 1;
  iput(cmip);
  pmip->dirty = 1;
  iput(pmip);
  return 1;
}

int my_readlink(char *file, char *buf)
{
  int ino;
  MINODE *mip;

  ino = getino(file);
  mip = iget(globalDev, ino);

  if ((mip->INODE.i_mode & 0xF000) != 0xA000)
    {
      printf("Error, %s is not a link\n", file);
    }

  get_block(globalDev, mip->INODE.i_block[0], buf);  
  // dp = (DIR *)buf;
  // char *cp = buf;

  // strcpy(buf, dp->name);
  //strcpy(buf, (char*)mip->INODE.i_block[0]);
  //THIS DOES NOT WORK

  return mip->INODE.i_size;
}

#endif
